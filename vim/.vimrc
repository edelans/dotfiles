
" ALE config
let g:ale_linters = {
      \   'python': ['flake8', 'pylint'],
      \}
let g:ale_fixers = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \    'python': ['yapf'],
      \}
nmap <F10> :ALEFix<CR>          " shortcut to fix
let g:ale_fix_on_save = 1
let g:ale_python_flake8_options = '--max-line-length=120'
let g:ale_completion_enabled = 1
let g:ale_completion_autoimport = 1         " automatic imports from external modules
let g:airline#extensions#ale#enabled = 1
let g:ale_set_highlights = 0

" Airline config
" needs font install https://powerline.readthedocs.io/en/master/installation.html#patched-fonts
let g:airline_powerline_fonts = 1

" virtualenv config
let g:virtualenv_auto_activate = 1

" Plugins
call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree'  " file system explorer
Plug 'kien/ctrlp.vim'      " Ctrl + P to start fuzzy search files
Plug 'tpope/vim-fugitive'  " Git integration
Plug 'vim-airline/vim-airline'          " Status bar
Plug 'dense-analysis/ale'  " syntax checking and semantic errors
" Plug 'neoclide/coc.nvim', {'branch': 'release'} " code completion
Plug 'https://github.com/airblade/vim-gitgutter'        " display diff markers
Plug 'https://github.com/jmcantrell/vim-virtualenv'     " helps with virtualenvs
Plug 'https://github.com/jiangmiao/auto-pairs'          " Insert or delete brackets, parens, quotes in pair.
Plug 'mbbill/undotree' " "visualizes undo history and makes it easier to browse and switch between different undo branches.
Plug 'maralla/completor.vim'        " Completion tool
Plug 'https://github.com/plasticboy/vim-markdown'       " for markdown files
call plug#end()


" NERD_tree config
let NERDTreeChDirMode=2
let NERDTreeIgnore=['\.vim$', '\~$', '\.pyc$', '\.swp$']
let NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$',  '\~$']
let NERDTreeShowBookmarks=1
map <C-n> :NERDTreeToggle<CR>

" open a NERDTree automatically when vim starts up if no files were specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" undo-tree config
nnoremap <F5> :UndotreeToggle<CR>   " nnoremap <F5> :UndotreeToggle<CR>



""""""""""""""""""""""""""""""""""""""""""""
" setup inspired from https://dougblack.io/words/a-good-vimrc.html
""""""""""""""""""""""""""""""""""""""""""""

" Colors
syntax on           " enable syntax processing

" spaces and tabs
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces
set smarttab        " Autotabs for certain code
set ruler           " Always shows location in file (line#)
set showmatch       " Shows matching brackets
set autoindent      " indent when moving to the next line while writing code

" UI Config
set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line
set showmatch           " highlight matching [{()}]
set history=1000
set laststatus=2        " status bar always on
set updatetime=100      " delay before vim writes its swap file and updates diff markers
let mapleader="\<Space>"  " define space as leader key

" cursor shape in xterm
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" Searching
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set ignorecase
nmap <Leader><esc> :noh<return>    " clear highlighting from a previous search


" open a file at the line you were when you closed it
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"z." |
     \ endif
